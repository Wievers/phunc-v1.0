import csv
import numpy as np
import matplotlib.pyplot as plt
import subprocess
import os
import os.path as pth
import shutil
import tempfile

from pylatex import Document, Tabular, Figure, Package, Center
from pylatex.table import Table

file = ""
valeurs = []

class Uncertainty:
    def __init__(self, rawLoc, tr = True):
        self.valAll = {} #L'index se fait avec des tuples (line, column)
        self.valAllIndex = None
        self.valUncertainty = {(0,0) : "/", (1,0) : "Valeur Moyenne", (2,0) : "Incertitude"}
        self.rawDatasLocation = rawLoc
        self.matricialTrans = tr
        self.convert()

    def __getitem__(self, value):
        if isinstance(value, tuple): return self.valAll[value]
        else: raise TypeError("Seuls les tuples sont acceptés")

    def convert(self):
        buffTab = []
        with open(self.rawDatasLocation, 'r') as f:
            csvConv = csv.reader(f) #Les titres sont OBLIGATOIRES
            for tab in csvConv: buffTab.append(tab)

        if len(buffTab) != 0 : self.valAllIndex = (len(buffTab)-1, len(buffTab[0])-1)
        else: raise Exception("Empty file")
            #Merci fabnem pour ce qui suit
        for line in range(len(buffTab)):
            for column in range(len(buffTab[line])):
                self.valAll[(line, column)] = buffTab[line][column] if line == 0 else float(buffTab[line][column])

        #if self.matricialTrans: buffTab = self.transpose(buffTab)
        #for tab in buffTab: self.valAll[tab[0]] = {"Values" : list(map(float, tab[1:]))}

    def transpose(self, matrice):
        """Prend un élément csv"""
        return zip(*matrice)

    def column(self, index, col = True, unc = False):
        columFinished = []
        if not unc : val = self.valAll
        else : val = self.valUncertainty
        for i in val.keys():
            if col:
                if i[1] == index: columFinished.append(val[i])
            else:
                if i[0] == index: columFinished.append(val[i])
        return columFinished

    def search(self, wres):
        cpt = 0
        for t in self.valAll.items():
            if t[1] == wres:
                cpt += 1
                index = t[0]
        if cpt > 1: raise Exception("Duplicates")
        else: return index #Si il y'a un duplicata de titre, ça devient problématique...

    def manual_unc(self):
        """Calcul manuel des incertitudes pour chaque valeur"""
    def tol_unc(self, tol):
        """Calcul statistique des incertitudes à partir d'une tolérance (Toutes les valeurs + ou - la tolérance
            seront prises en comptes pour faire l'incertitude)"""
    def formula_unc(self):
        """Calcul des incertitudes à partir de la dérivée logarithmique"""
    def calcUnc(self, nbCol, nbSign = 3):
        valAll = self.column(nbCol)
        self.valUncertainty[(0, nbCol + 1)] = valAll.pop(0)
        buffSum = sum(valAll)
        self.valUncertainty[(1, nbCol + 1)] = 1/len(valAll)*buffSum #On calcule notre valeur moyenne
        buffer = sum(((val - self.valUncertainty[(1, nbCol + 1)])**2 for val in valAll))
        buffSigma = ((1/(len(valAll)-1))*buffer)**0.5 #On calcule le sigma pour l'incertitude
        self.valUncertainty[(2, nbCol + 1)] = round(buffSigma / (len(valAll))**0.5, nbSign) #On calcule notre incertitude

class genLatex:
    def __init__(self, datasToLat):
        #raw_datas <=> instance Uncertainty
        self.raw_datas = datasToLat
        self.genPartialTex = ""
        self.genPartialImageTex = ""
        self.graphs = []
        #Configuration du document Latex
        self.fileSets()

        #Configuration administrative des fichiers du programme
        self.rootLatGen = pth.join(os.getcwd(),"generatedFiles")
        if not pth.isdir(self.rootLatGen): os.mkdir(self.rootLatGen)

    def fileSets(self):
        self.genDoc = Document()
        self.genDoc.packages.append(Package("float"))
        self.genDoc.packages.append(Package("inputenc", "utf8"))

    def rootFile(self, file = None):
        """Method returning the name of a file in the directory specified
        in the settings for generation (It doesn't verify whether or not the file exists
        in the directory)
            - file : The name of the file [opt]"""
        if file is not None:
            return pth.join(self.rootLatGen, file)
        elif file is None:
            return self.rootLatGen
        else:
            raise TypeError("The name of the file should be a str")

    def genTab(self, titleDoc, unc = False):
        """This method generate a table with the datas processed
            - titleDoc : The title of the table, which is a str
            - unc : Table for Uncertainties"""
        if unc:
            dynInd = [i[1] for i in self.raw_datas.valUncertainty]
            index = (2, max(dynInd) + 1)
            val = self.raw_datas.valUncertainty
            titles = self.raw_datas.column(0, False, unc)

        else:
            val = self.raw_datas.valAll
            titles = self.raw_datas.column(0, False, unc)
            index = self.raw_datas.valAllIndex

        colCod = "l"
        for i in range(len(titles)-1): colCod += '|c'
        finalTab = Table(position = "H")
        table = Tabular(colCod)
        table.add_row(titles)
        table.add_hline()
        for line in range(1, index[0] + 1):
            table.add_row(self.raw_datas.column(line, False, unc))
            table.add_hline()
        centeredTab = Center()
        centeredTab.append(table)
        finalTab.append(centeredTab)
        finalTab.add_caption(titleDoc)
        return finalTab

    def genGraph(self, colx, coly, graphTitle, graphFileName = None, errorsX = None, errorsY = None):
        """Method generating a graph (png) and inserting it in a Latex file generation
            - colx : The column of the table that will be used as the x axis (int)
            - coly : The column of the table that will be used as the y axis (int)
            - graph_title : The title of the graph (str)
            - errorsX : The error on the x axis (default : None)
            - errorsY : The error on the y axis (default : None)
           return nothing"""
        #File parameter

        if type(graphFileName) is str:
            graphFile = "{}/{}.png".format(self.rootLatGen, graphFileName)

        elif graphFileName is None:
            graphFile = "{}/{}.png".format(self.rootLatGen, graphTitle)
        else:
            raise TypeError("Name should be a string")

        #Extracting the x and y columns of the dictionnary
        x_title = self.raw_datas.column(colx)[0]
        x = self.raw_datas.column(colx)[1:] #We don't want the title, hence [1:]
        y_title = self.raw_datas.column(coly)[0]
        y = self.raw_datas.column(coly)[1:]

        #Creating the error, for the graph
        if errorsX is not None: x_error = self.raw_datas.column(errorsX)[1:]
        if errorsY is not None: y_error = self.raw_datas.column(errorsY)[1:]

        #Let's plot!
        t = np.polyfit(x, y, 1)
        print(np.polyfit(x, y, 1))
        print(np.poly1d(t))
        plt.xlabel(x_title)
        plt.ylabel(y_title)
        plt.title(graphTitle)
        if errorsX is not None: plt.errorbar(x, y, xerr = x_error, linestyle='dotted')
        if errorsY is not None: plt.errorbar(x, y, yerr = y_error, linestyle='dotted')
        plt.savefig(graphFile)
        plt.close()

        graph = Figure(position = "b")
        graph.add_image(graphFile)
        graph.add_caption(graphTitle)
        centeredGraph = Center()
        centeredGraph.append(graph)
        self.graphs.append(graph)

    def genPartTex(self, title, compile = False):
        """Method generating the final Tex output.
            - title : The title of your soon to be document (str)
            - compile : Create a Pdf (True) or a tex file (False)
        """
        self.fileSets()
        self.genDoc.append(self.genTab(title, False))
        self.genDoc.append(self.genTab("Uncertainty", True))
        #On ajoute les graphes générés
        for i in self.graphs:
            self.genDoc.append(i)

        if compile:
            self.genDoc.generate_pdf(pth.join(self.rootLatGen, title))
        else:
            self.genDoc.generate_tex(pth.join(self.rootLatGen, title))
            f = open(pth.join(self.rootLatGen, "{}.tex".format(title)))
            return f.read()

    def genStandaloneTex(self):
        """Method generating a PDF by compiling LaTeX generation using system call"""

        completeTex = "\\documentclass{article}\\usepackage{float}\\usepackage{graphicx}\\begin{document}"+ self.genPartialTex + self.genPartialImageTex +"\\end{document}"
        temp = tempfile.mkdtemp()
        os.chdir(temp)

        f = open('pdfTest.tex','w')
        f.write(completeTex)
        f.close()

        proc=subprocess.Popen(['pdflatex','pdfTest.tex'])
        FNULL = open(os.devnull, 'w')
        retcode = subprocess.call(['pdflatex', completeTex], stdout=FNULL, stderr=subprocess.STDOUT)
        #subprocess.Popen(['pdflatex', completeTex])
        proc.communicate()

        os.rename('pdfTest.pdf',"pdfFinal.pdf")
        shutil.copy("pdfFinal.pdf", self.rootLatGen)
        shutil.rmtree(temp)
        #il faut faire attention à la casse
        #sinon ça casse le programme
