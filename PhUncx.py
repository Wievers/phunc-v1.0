import sys
import json as js
import ntpath as ntp
from PySide2 import QtWidgets as qtw
from PySide2.QtCore import Qt, Slot, QAbstractItemModel
from PySide2 import QtGui as qtg

import calculInc as cnc

def nomImage():
  i = 0
  while True:
    yield "image{}".format(i)
    i += 1

class adminWindow(qtw.QDialog):
    def __init__(self, aide, creator, licence, parent = None):
        super(adminWindow, self).__init__(parent)
        layout = qtw.QVBoxLayout()
        self.aideText = qtw.QLabel(aide)
        self.creator = qtw.QLabel(creator)
        self.licence = qtw.QLabel(licence)

        layout.addWidget(self.aideText)
        layout.addWidget(self.creator)
        layout.addWidget(self.licence)
        self.setLayout(layout)

class uncAsk(qtw.QDialog):
    def __init__(self, listNames, parent = None):
        super(uncAsk, self).__init__(parent)
        layout = qtw.QVBoxLayout()
        nbLayouts = len(listNames) // 6 + 1 #Number of rows to stack 6 buttons per row
        self.buttonClicked = [] #The titles of the boxes that have been checked
        self.listNames = listNames

        #Confirmation button
        self.conf = qtw.QPushButton("Calculate")
        self.conf.clicked.connect(self.checked)

        #Buttons Layout
        HLayouts = [qtw.QHBoxLayout() for i in range(nbLayouts)]
        self.buttons = [qtw.QCheckBox(i) for i in listNames]
        self.buttons.insert(0, qtw.QCheckBox("All"))
        self.buttons[0].pressed.connect(self.allPressed)
        comptLay = 0
        for j in range(len(self.buttons)):
            if j == 0 or j == 1 or j % 6 != 0: #The 0th and 1st buttons are displayed on the first line
                HLayouts[comptLay].addWidget(self.buttons[j])
            else:
                comptLay += 1
                HLayouts[comptLay].addWidget(self.buttons[j])

        #Layout display
        for i in HLayouts: layout.addLayout(i)
        layout.addWidget(self.conf)
        self.setLayout(layout)

    @Slot()
    def allPressed(self):
        if not self.buttons[0].isChecked():
            for i in self.buttons[1:]:
                i.setChecked(True)
                i.setEnabled(False)
        else:
            for i in self.buttons[1:]:
                i.setChecked(False)
                i.setEnabled(True)

    @Slot()
    def checked(self):
        if self.buttons[0].isChecked(): self.buttonClicked = self.listNames
        else:
            for i in self.buttons[1:]:
                if i.isChecked() : self.buttonClicked.append(i.text())
        self.close()

class mainWindow(qtw.QWidget):
    def __init__(self):
        super(mainWindow, self).__init__()
        mainLayout = qtw.QVBoxLayout()
        self.window().setWindowTitle("PhUncX v1.0")
        self.setMinimumSize(750,200)

        #Paramétrage des langues i18n
        self.configFileLang = "lang_fr.json"
        self.langSets = None
        self.extractLang()

        #Variables admins
        self.datas = None
        self.datasLat = None
        self.title = ""
        self.graphGenerated = []
        self.uncGenerated = []
        self.graphName = nomImage()

        #1 explo de fichier rapide
        firstButLayout = qtw.QHBoxLayout()
        self.exploFile = qtw.QPushButton(self.langSets["exploFichier"])
        self.exploFile.setToolTip("The csv from which we will extract your experience's datas")
        self.exploFile.clicked.connect(self.browse)
        self.helpButton = qtw.QPushButton()
        icon = qtg.QIcon()
        icon.addPixmap(qtg.QPixmap("graphics/gear.png"))
        self.helpButton.setIcon(icon)
        self.helpButton.setToolTip("Aide")
        self.helpButton.setFixedSize(40, 40)
        self.helpButton.clicked.connect(self.help)
        firstButLayout.addWidget(self.exploFile)
        firstButLayout.addWidget(self.helpButton)

        #Calcul Toutes incertitudes
        self.allUncButton = qtw.QPushButton(self.langSets["incertitudes"])
        self.allUncButton.setToolTip("Choose the uncertainties to calculate")
        self.allUncButton.clicked.connect(self.uncCalc)
        self.allUncButton.setEnabled(False)

        #Titre
        titleLayout = qtw.QHBoxLayout()
        self.titleWid = qtw.QLineEdit()
        self.titleConf = qtw.QPushButton(self.langSets["sauvegarde"])
        self.titleConf.clicked.connect(self.titleSel)
        titleLayout.addWidget(self.titleWid)
        titleLayout.addWidget(self.titleConf)

        #2 menus (axe y, axe x) pour le graphique
        axisLayout = qtw.QHBoxLayout()
        errorsLayout = qtw.QHBoxLayout()
        self.XAxis = qtw.QToolButton(self)
        self.XAxis.setToolTip("X-Axis")
        self.XAxis.setPopupMode(qtw.QToolButton.MenuButtonPopup)
        self.XAxis.setFixedSize(300, 30)
        self.XAxis.setMenu(qtw.QMenu(self.XAxis))
        axisLayout.addWidget(self.XAxis)

        self.XAxisError = qtw.QToolButton(self)
        self.XAxisError.setToolTip("X-Axis Uncertainty")
        self.XAxisError.setPopupMode(qtw.QToolButton.MenuButtonPopup)
        self.XAxisError.setFixedSize(300, 30)
        self.XAxisError.setMenu(qtw.QMenu(self.XAxisError))
        errorsLayout.addWidget(self.XAxisError)


        self.YAxis = qtw.QToolButton(self)
        self.YAxis.setToolTip("Y-Axis")
        self.YAxis.setPopupMode(qtw.QToolButton.MenuButtonPopup)
        self.YAxis.setFixedSize(300, 30)
        self.YAxis.setMenu(qtw.QMenu(self.YAxis))
        axisLayout.addWidget(self.YAxis)

        self.YAxisError = qtw.QToolButton(self)
        self.YAxisError.setToolTip("Y-Axis Uncertainty")
        self.YAxisError.setPopupMode(qtw.QToolButton.MenuButtonPopup)
        self.YAxisError.setFixedSize(300, 30)
        self.YAxisError.setMenu(qtw.QMenu(self.YAxisError))
        errorsLayout.addWidget(self.YAxisError)

        #Tracage de graphique
        self.drawGraph = qtw.QPushButton(self.langSets["graphique"])
        self.drawGraph.setToolTip("Draw the graphic with the x-axis and y-axis defined earlier")
        self.drawGraph.setEnabled(False)
        self.drawGraph.clicked.connect(self.draw)

        #1 lecteur de graph (png)
        viewLayout = qtw.QHBoxLayout()
        self.graphViewLabel = qtw.QLabel()
        self.texShow = qtw.QTextEdit()
        self.texShow.setVisible(False)
        viewLayout.addWidget(self.graphViewLabel)
        viewLayout.addWidget(self.texShow)

        #1 lecteur de CSV
        #Générer Un pdf/Latex
        genLayout = qtw.QHBoxLayout()
        self.genPdf = qtw.QPushButton(self.langSets["generationPDF"])
        self.genPdf.setEnabled(False)
        self.genPdf.clicked.connect(self.savePdf)
        self.genLatex = qtw.QPushButton(self.langSets["generationTex"])
        self.genLatex.setEnabled(False)
        self.genLatex.clicked.connect(self.showLatex)
        genLayout.addWidget(self.genPdf)
        genLayout.addWidget(self.genLatex)

        #Orga finale
        mainLayout.addLayout(firstButLayout)
        mainLayout.addLayout(titleLayout)
        mainLayout.addWidget(self.allUncButton)
        mainLayout.addLayout(axisLayout)
        mainLayout.addLayout(errorsLayout)
        mainLayout.addWidget(self.drawGraph)
        mainLayout.addLayout(viewLayout)
        mainLayout.addLayout(genLayout)
        self.setLayout(mainLayout)

    def extractLang(self):
        config = open(self.configFileLang, "r")
        data = config.read()
        self.langSets = js.loads(data)
        config.close()

    def help(self):
        theHelp = adminWindow(self.langSets["aideCSV"], self.langSets["creator"], self.langSets["licence"])
        theHelp.exec_()

    @Slot()
    def titleSel(self):
        self.title = self.titleWid.text()
        #On active la génération
        if self.title != "" and self.exploFile.text() != self.langSets["exploFichier"]:
            if self.YAxis.text() != "" and self.XAxis.text() != "" and self.YAxis.text() != self.XAxis.text(): self.drawGraph.setEnabled(True)
            self.genPdf.setEnabled(True)
            self.genLatex.setEnabled(True)
            self.allUncButton.setEnabled(True)
        else:
            self.genPdf.setEnabled(False)
            self.genLatex.setEnabled(False)
            self.drawGraph.setEnabled(False)
            self.allUncButton.setEnabled(False)

    @Slot()
    def browse(self):
        #On va chercher le csv
        files = qtw.QFileDialog()
        test = files.getOpenFileNames(self, "Select a csv to open", "/home/wieves/Documents/Travail/Physique/phys210/Graph", "*.csv")
        #On prend acte du choix de l'utilisateur
        buffi = ntp.basename(test[0][0]).rfind(".")
        self.exploFile.setText(ntp.basename(test[0][0]))
        self.titleWid.setText(ntp.basename(test[0][0])[:buffi])
        self.datas = cnc.Uncertainty(test[0][0])
        self.datasLat = cnc.genLatex(self.datas)
        #On implémente le résultat dans le programme
        textBox = [qtw.QListWidget(self) for t in range(4)]
        data_title = self.datas.column(0, False)
        for t in data_title:
            textBox[0].addItem(qtw.QListWidgetItem(t))
            textBox[1].addItem(qtw.QListWidgetItem(t))
            textBox[2].addItem(qtw.QListWidgetItem(t))
            textBox[3].addItem(qtw.QListWidgetItem(t))

        textBox[0].itemClicked.connect(lambda item : self.axis(item, True))
        textBox[1].itemClicked.connect(lambda item : self.axis(item, False))
        textBox[2].itemClicked.connect(lambda item : self.axis(item, None, True))
        textBox[3].itemClicked.connect(lambda item : self.axis(item, None, False))

        action1 = qtw.QWidgetAction(self.XAxis)
        action1.setDefaultWidget(textBox[0])
        self.XAxis.menu().addAction(action1)
        action2 = qtw.QWidgetAction(self.YAxis)
        action2.setDefaultWidget(textBox[1])
        self.YAxis.menu().addAction(action2)
        action3 = qtw.QWidgetAction(self.XAxisError)
        action3.setDefaultWidget(textBox[2])
        self.XAxisError.menu().addAction(action3)
        action4 = qtw.QWidgetAction(self.YAxisError)
        action4.setDefaultWidget(textBox[3])
        self.YAxisError.menu().addAction(action4)

    @Slot()
    def axis(self, title, x = True, x_err = None):
        if x and x is not None:
            self.XAxis.setText(title.text())
            self.XAxis.menu().hide()
        elif not x and x is not None:
            self.YAxis.setText(title.text())
            self.YAxis.menu().hide()
        if x_err and x_err is not None:
            self.XAxisError.setText(title.text())
            self.XAxisError.menu().hide()
        elif not x_err and x_err is not None:
            self.YAxisError.setText(title.text())
            self.YAxisError.menu().hide()

        if self.title != "" and self.YAxis.text() != "" and self.XAxis.text() != "" and self.YAxis.text() != self.XAxis.text(): self.drawGraph.setEnabled(True)
        else: self.drawGraph.setEnabled(False)

    @Slot()
    def draw(self):
        #On vérifie que l'on ai pas déjà généré un tel graphe
        if (self.XAxis.text(), self.YAxis.text(), self.XAxisError.text(), self.YAxisError.text()) not in self.graphGenerated:
            buffName = next(self.graphName)
            if self.XAxisError.text() != "" and self.YAxisError.text() != "":
                self.datasLat.genGraph(self.datas.search(self.XAxis.text())[1], self.datas.search(self.YAxis.text())[1], self.title, buffName, self.datas.search(self.XAxisError.text())[1], self.datas.search(self.YAxisError.text())[1])
            elif self.XAxisError.text() != "":
                self.datasLat.genGraph(self.datas.search(self.XAxis.text())[1], self.datas.search(self.YAxis.text())[1], self.title, buffName, errorsX = self.datas.search(self.XAxisError.text())[1])
            elif self.YAxisError.text() != "":
                self.datasLat.genGraph(self.datas.search(self.XAxis.text())[1], self.datas.search(self.YAxis.text())[1], self.title, buffName, errorsY = self.datas.search(self.YAxisError.text())[1])
            else:
                self.datasLat.genGraph(self.datas.search(self.XAxis.text())[1], self.datas.search(self.YAxis.text())[1], self.title, buffName, False)
            #On initialise le lecteur de graphe
            graphView = qtg.QPixmap()
            graphView.load(self.datasLat.rootFile(buffName))
            self.graphViewLabel.setPixmap(graphView)

            #On ajoute ce graphe à la liste des graphes générés, histoire de pas le générer en double
            self.graphGenerated.append((self.XAxis.text(), self.YAxis.text(), self.XAxisError.text(), self.YAxisError.text()))

    @Slot()
    def showLatex(self):
        tex = self.datasLat.genPartTex(self.title)
        self.texShow.setPlainText(tex)
        self.texShow.setVisible(True)

    @Slot()
    def savePdf(self):
        self.datasLat.genPartTex(title = self.title, compile = True)

    @Slot()
    def uncCalc(self):
        uncWanted = uncAsk(self.datas.column(0, False), self)
        uncWanted.exec_()
        for i in uncWanted.buttonClicked: self.datas.calcUnc(self.datas.search(i)[1])
        self.uncGenerated = uncWanted.buttonClicked

if __name__ == '__main__':
    # Create the Qt Application
    app = qtw.QApplication(sys.argv)
    # Create and show the form
    form = mainWindow()
    form.show()
    # Run the main Qt loop
    sys.exit(app.exec_())
