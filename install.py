import sys
import subprocess

RED = "\033[31m"
GREEN = "\033[32m"
YELLOW = "\033[33m"
BLUE = "\033[34m"
CYAN = "\033[36m"
NEUTRAL = "\033[0m"
BOLD = "\033[1m"
NORMAL = "\033[0m"

packages = ["PySide2", "matplotlib", "PyLatex"]
# implement pip as a subprocess:
print(BOLD + RED + "Will be installed at the end of the script:" + NEUTRAL + NORMAL)
print(BOLD + "{}".format(packages[0]) + NORMAL + " : The GUI library Qt which manage all the windows and interface stuff ")
print(BOLD + "{}".format(packages[1]) + NORMAL + " : A library to make pretty graphs for statistic needs")
print(BOLD + "{}".format(packages[2]) + NORMAL + " : A LaTeX generator library")
print()

for i in packages:
    print(BOLD + RED + i + NEUTRAL + NORMAL)
    subprocess.check_call([sys.executable, '-m', 'pip3', 'install', i])

    # process output with an API in the subprocess module:
    reqs = subprocess.check_output([sys.executable, '-m', 'pip', 'freeze'])
    print()

print(BOLD + "Done! Have a nice day, user" + NORMAL)
